package com.test;

import java.util.Scanner;

public class Problem8 {
public static void main(String [] args){
	Scanner user_input = new Scanner (System.in);
	int n= user_input.nextInt();
	
	int i, j;
	
	boolean numarPrim;
	for (i = 1; i <= n; i++){
		numarPrim = true;
		for (j = 2; j < i; j++){
			if (i%j == 0){
				numarPrim = false;
				break;
			}
		}
		if (numarPrim){
			System.out.println(i+" ") ;
		}
	}
}
}